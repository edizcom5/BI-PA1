#include <stdio.h>
#include <float.h>

int get_vars(short int num, double * first, double * second) {
    if (!num) {
        printf("Hmotnost a koncentrace vysledku:\n");
    } else {
        printf("Hmotnost a koncentrace #%d:\n", num);
    }

    if (scanf("%lf %lf", first, second) != 2 ||
        *first < 0 ||
        *second < 0 ||
        *second > 1) {
        printf("Nespravny vstup.\n");
        return 0;
    } else {
        return 1;
    }

}


int get_solution_for(double v1, double c1, double v2, double c2,
                     double v3, double c3, double vv, double cv, double * r1, double * r2, double * r3) {

    double vxr, ri1, ri2, ri3;

    ri1 = -((c2*v2-cv*v2+c3*v3-cv*v3)*vv)/((c1*v2)-(c2*v2)+(c1*v3)-(c3*v3));
    vxr = ((c1*v2-cv*v2+c1*v3-cv*v3)*vv)/((c1*v2)-(c2*v2)+(c1*v3)-(c3*v3));
    ri2 = (v2*vxr)/(v2+v3);
    ri3 = (v3*vxr)/(v2+v3);

    if (ri1 - v1 > 10 * FLT_EPSILON * v1 || ri2 - v2 > 10 * FLT_EPSILON * v2 || ri3 - v3 > 10 * FLT_EPSILON * v3 || ri1 < 0 || ri2 < 0 || ri3 < 0) {
        return 0;
    } else {
        *r1 = ri1;
        *r2 = ri2;
        *r3 = ri3;
        return 1;
    }

}

int main() {
    int i, x;
    double v1, c1, v2, c2, v3, c3, vv, cv, r1, r2, r3;

    if (! get_vars(1, &v1, &c1)) return 0;
    if (! get_vars(2, &v2, &c2)) return 0;
    if (! get_vars(3, &v3, &c3)) return 0;
    if (! get_vars(0, &vv, &cv)) return 0;

    if (c1 == cv && c2 == cv && c3 == cv) {
        if (v1 > vv) {
            r1 = vv;
            r2 = 0;
            r3 = 0;
        } else if (v2 > vv) {
            r2 = vv;
            r1 = 0;
            r3 = 0;
        } else if (v3 > vv) {
            r3 = vv;
            r2 = 0;
            r1 = 0;
        } else {
            r1 = v1;
            if (vv - r1 > v2) {
                r2 = v2;
            } else {
                r2 = vv - r1;
            }
            r3 = vv - r1 - r2;
        }
    } else {
        for (i = 0; i < 5; ++i) {
            if (i == 0) x = get_solution_for(v1, c1, v2, c2, v3, c3, vv, cv, &r1, &r2, &r3); /* V1 V2 V3 */
            if (i == 1) x = get_solution_for(v1, c1, v3, c3, v2, c2, vv, cv, &r1, &r3, &r2); /* V1 V3 V2 */
            if (i == 2) x = get_solution_for(v2, c2, v1, c1, v3, c3, vv, cv, &r2, &r1, &r3); /* V2 V1 V3 */
            if (i == 3) x = get_solution_for(v2, c2, v3, c3, v1, c1, vv, cv, &r2, &r3, &r1); /* V2 V3 V1 */
            if (i == 4) x = get_solution_for(v3, c3, v1, c1, v2, c2, vv, cv, &r3, &r1, &r2); /* V3 V1 V2 */
            if (i == 5) x = get_solution_for(v3, c3, v2, c2, v1, c1, vv, cv, &r2, &r2, &r1); /* V3 V2 V1 */
            if (x) {
                break;
            }
        }
        if (!x) {
            printf("Nelze dosahnout.\n");
            return 0;
        }

    }

    if (r1 - v1 > 10 * FLT_EPSILON * v1 || r2 - v2 > 10 * FLT_EPSILON * v2 || r3 - v3 > 10 * FLT_EPSILON * v3 || r1 < 0 || r2 < 0) {
        printf("Nelze dosahnout.\n");
        return 0;
    }

    printf("%.6f x #1 + %.6f x #2 + %.6f x #3\n", r1, r2, r3);

    return 0;
}