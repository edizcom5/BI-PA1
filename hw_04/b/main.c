#include <stdio.h>
#include <stdlib.h>

#define EMPTY 0
#define HEALTHY 1
#define SICK 2

typedef struct sickLocation {
    short int x, y;
} SICKLOCATION;

int sicks = 0, days = 0, a, b;
char office[500][500];
SICKLOCATION *  sickos = NULL;
int sickos_count = 0;
int sickos_allocated = 0;



void print_incorrect_entry() {
    /*printf("%d %d", a, b);*/
    printf("Nespravny vstup.\n");
}

void print_no_sicks() {
    printf("Nikdo neonemocnel.\n");
}

void print_sicks() {
    printf("Nakazenych: %d, doba sireni nakazy: %d\n", sicks, days);
}

SICKLOCATION * add_sicko(SICKLOCATION * where, int * count, int * allocated, short i, short j) {
    ++*count;

    if (where == NULL) {
        *allocated = *count * 2;
        where = (SICKLOCATION *) malloc(*allocated * sizeof( SICKLOCATION));
    } else {
        *allocated = *count * 2;
        where = (SICKLOCATION *) realloc (where, *allocated * sizeof(SICKLOCATION));
    }
    where[*count-1].x = i;
    where[*count-1].y = j;
    return where;
}

short int validate_char(char x) {
    if (x == '.') return EMPTY;
    else if (x == 'o') return HEALTHY;
    else if (x == '!') return SICK;
    else return -1;
}

short int get_office() {
    short i, j, temp;
    char current;
    for(i = 0; i < a; ++i) {
        for(j = 0; j < b; ++j) {
            if (scanf("%c", &current) != 1) return 0;
            temp = validate_char(current);
            if (temp == -1) return 0;
            office[i][j] = temp;
            if (temp == SICK) {
                ++sicks;
                sickos = add_sicko(sickos, &sickos_count, &sickos_allocated, i, j);
            }
        }

        if (scanf("%c", &current) != 1 || current != '\n') return 0;
    }

    if (scanf("%c", &current) != -1) return 0;
    return 1;
}

void print_office() {
    short i, j;

    printf("\nOffice:\n");

    for(i = 0; i < a; ++i) {
        for(j = 0; j < b; ++j) {
            if (office[i][j] == EMPTY) printf(".");
            else if (office[i][j] == HEALTHY) printf("o");
            else if (office[i][j] == SICK) printf("!");
        }

        printf("\n");
    }

}

void print_sickos() {
    int i;
    for (i = 0; i < sickos_count; ++i) {
        if (i != 0) printf(", ");
        printf("%hd %hd", sickos[i].x, sickos[i].y);
    }
    printf("\n");
}


void sick_them_all() {
    SICKLOCATION *  new_sickos = NULL;
    int new_sickos_count = 0, new_sickos_allocated = 0, i;
    short x, y;
    int old_sicks = sicks;

    for (i = 0; i < sickos_count; ++i) {
        x = sickos[i].x;
        y = sickos[i].y;

        if (x+1 >= 0 && x+1 <= a && y >= 0 && y <= b && office[x+1][y] == HEALTHY) {
            ++sicks;
            new_sickos = add_sicko(new_sickos, &new_sickos_count, &new_sickos_allocated, x+1, y);
            office[x+1][y] = SICK;
        }
        if (x-1 >= 0 && x-1 <= a && y >= 0 && y <= b && office[x-1][y] == HEALTHY) {
            ++sicks;
            new_sickos = add_sicko(new_sickos, &new_sickos_count, &new_sickos_allocated, x-1, y);
            office[x-1][y] = SICK;
        }
        if (x >= 0 && x <= a && y+1 >= 0 && y+1 <= b && office[x][y+1] == HEALTHY) {
            ++sicks;
            new_sickos = add_sicko(new_sickos, &new_sickos_count, &new_sickos_allocated, x, y+1);
            office[x][y+1] = SICK;
        }
        if (x >= 0 && x <= a && y-1 >= 0 && y-1 <= b && office[x][y-1] == HEALTHY) {
            ++sicks;
            new_sickos = add_sicko(new_sickos, &new_sickos_count, &new_sickos_allocated, x, y-1);
            office[x][y-1] = SICK;
        }
    }

    if (old_sicks != sicks) ++days;
    free(sickos);
    sickos = new_sickos;
    sickos_count = new_sickos_count;
    sickos_allocated = new_sickos_allocated;
}

int main(void) {
    printf("Velikost kancelare:\n");

    if (scanf("%d %d", &a, &b) != 2 || a < 1 || a > 500 || b < 1 || b > 500) {
        print_incorrect_entry();
        return 0;
    }

    while ( getchar() != '\n' );

    if (!get_office()) {
        print_incorrect_entry();
        free(sickos);
        return 0;
    }

    if (sicks == 0) {
        print_no_sicks();
        return 0;
    }

    do {
        /*printf("\n\nSickos count: %d\n", sickos_count);*/
        sick_them_all();
        /*print_office();*/
        /*print_sickos();*/

    } while(sickos_count != 0);

    print_sicks();

    free(sickos);
    return 0;
}