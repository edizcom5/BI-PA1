#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

typedef struct TPage
 {
   char         ** m_Lines;
   int             m_LinesNr;
 } TPAGE;

typedef struct TDocument
 {
   TPAGE         * m_Pages;
   int             m_PagesNr;
 } TDOCUMENT;

#endif /* __PROGTEST__ */

TDOCUMENT        * DocFormat                               ( int          pageWidth,
                                                             int          pageRows,
                                                             const char * text );

void               DocFree                                 ( TDOCUMENT  * x );

#ifndef __PROGTEST__
int main ( int argc, char * argv [] )
 {
   TDOCUMENT * doc;

   doc = DocFormat ( 30, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );
   assert ( doc -> m_PagesNr == 5 );
   assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "consectetur adipiscing elit." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "In tincidunt posuere diam in" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "elementum.  Proin viverra" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "risus nibh,  eu ornare est" ) == 0 );
   assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "placerat ut.  Maecenas nec" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "nisl non elit pharetra" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "finibus.  Nunc ut rutrum enim." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "Mauris congue,  neque quis" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "porta porta,  augue erat" ) == 0 );
   assert ( doc -> m_Pages[2] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "sodales ligula,  ac blandit" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "risus odio quis tellus." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "   Sed a dictum eros,  sed" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "posuere nibh.  Nulla sodales" ) == 0 );
   assert ( doc -> m_Pages[3] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[0], "sit amet nunc vestibulum" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[1], "rutrum.  Vivamus venenatis," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[2], "quam et iaculis bibendum," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[3], "dolor felis pretium dolor,  at" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[4], "viverra lacus erat in sapien." ) == 0 );
   assert ( doc -> m_Pages[4] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[4] . m_Lines[0], "Donec accumsan nulla massa," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[4] . m_Lines[1], "sed facilisis nulla efficitur" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[4] . m_Lines[2], "at.  Sed tellus tellus," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[4] . m_Lines[3], "mattis et pulvinar vel," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[4] . m_Lines[4], "dignissim at nulla." ) == 0 );
   DocFree ( doc );

   doc = DocFormat ( 50, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n"  );
   assert ( doc -> m_PagesNr == 4 );
   assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet,  consectetur" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "adipiscing elit.  In tincidunt posuere diam in" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "elementum.  Proin viverra risus nibh,  eu ornare" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "est placerat ut.  Maecenas nec nisl non elit" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "pharetra finibus.  Nunc ut rutrum enim.  Mauris" ) == 0 );
   assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "congue,  neque quis porta porta,  augue erat" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "sodales ligula,  ac blandit risus odio quis" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "tellus." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "   Sed a dictum eros,  sed posuere nibh.  Nulla" ) == 0 );
   assert ( doc -> m_Pages[2] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "sodales sit amet nunc vestibulum rutrum.  Vivamus" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "venenatis,  quam et iaculis bibendum,  dolor felis" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "pretium dolor,  at viverra lacus erat in sapien." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "Donec accumsan nulla massa,  sed facilisis nulla" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "efficitur at.  Sed tellus tellus,  mattis et" ) == 0 );
   assert ( doc -> m_Pages[3] . m_LinesNr == 1 );
   assert ( strcmp ( doc -> m_Pages[3] . m_Lines[0], "pulvinar vel,  dignissim at nulla." ) == 0 );
   DocFree ( doc );

   doc = DocFormat ( 80, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );
   assert ( doc -> m_PagesNr == 3 );
   assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet,  consectetur adipiscing elit.  In tincidunt" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "posuere diam in elementum.  Proin viverra risus nibh,  eu ornare est placerat" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "ut.  Maecenas nec nisl non elit pharetra finibus.  Nunc ut rutrum enim.  Mauris" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "congue,  neque quis porta porta,  augue erat sodales ligula,  ac blandit risus" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "odio quis tellus." ) == 0 );
   assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "   Sed a dictum eros,  sed posuere nibh.  Nulla sodales sit amet nunc vestibulum" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "rutrum.  Vivamus venenatis,  quam et iaculis bibendum,  dolor felis pretium" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "dolor,  at viverra lacus erat in sapien.  Donec accumsan nulla massa,  sed" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "facilisis nulla efficitur at.  Sed tellus tellus,  mattis et pulvinar vel," ) == 0 );
   assert ( doc -> m_Pages[2] . m_LinesNr == 1 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "dignissim at nulla." ) == 0 );
   DocFree ( doc );
   doc = DocFormat ( 20, 15, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );
   assert ( doc -> m_PagesNr == 3 );
   assert ( doc -> m_Pages[0] . m_LinesNr == 15 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "sit amet," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "consectetur" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "adipiscing elit.  In" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "tincidunt posuere" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[5], "diam in elementum." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[6], "Proin viverra risus" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[7], "nibh,  eu ornare est" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[8], "placerat ut." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[9], "Maecenas nec nisl" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[10], "non elit pharetra" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[11], "finibus.  Nunc ut" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[12], "rutrum enim.  Mauris" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[13], "congue,  neque quis" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[0] . m_Lines[14], "porta porta,  augue" ) == 0 );
   assert ( doc -> m_Pages[1] . m_LinesNr == 15 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "erat sodales ligula," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "ac blandit risus" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "odio quis tellus." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "   Sed a dictum" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[5], "eros,  sed posuere" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[6], "nibh.  Nulla sodales" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[7], "sit amet nunc" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[8], "vestibulum rutrum." ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[9], "Vivamus venenatis," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[10], "quam et iaculis" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[11], "bibendum,  dolor" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[12], "felis pretium dolor," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[13], "at viverra lacus" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[1] . m_Lines[14], "erat in sapien." ) == 0 );
   assert ( doc -> m_Pages[2] . m_LinesNr == 8 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "Donec accumsan nulla" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "massa,  sed" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "facilisis nulla" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "efficitur at.  Sed" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "tellus tellus," ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[5], "mattis et pulvinar" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[6], "vel,  dignissim at" ) == 0 );
   assert ( strcmp ( doc -> m_Pages[2] . m_Lines[7], "nulla." ) == 0 );
   DocFree ( doc );

   assert ( DocFormat ( 10, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" ) == NULL );
   assert ( DocFormat ( 8, 5, "Lorem." ) == NULL );
   return 0;
 }
#endif /* __PROGTEST__ */
