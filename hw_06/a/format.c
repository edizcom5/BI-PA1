#ifndef __PROGTEST__
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <ctype.h>

typedef struct
 {
   char ** m_Lines;
   int m_LinesNr;
 } TPAGE;

typedef struct
 {
   TPAGE  * m_Pages;
   int m_PagesNr;
 } TDOCUMENT;

#endif /* __PROGTEST__ */


void add_page(TDOCUMENT * document, int page_width, int page_rows) {
    int i;
    document->m_PagesNr += 1;
    document->m_Pages = (TPAGE *) realloc(document->m_Pages, document->m_PagesNr*sizeof(TPAGE));
    document->m_Pages[document->m_PagesNr-1].m_LinesNr = 0;
    document->m_Pages[document->m_PagesNr-1].m_Lines = (char **) malloc(page_rows * sizeof(char *));
    for (i = 0; i < page_rows; ++i) {
        document->m_Pages[document->m_PagesNr-1].m_Lines[i] = NULL;
    }
}

int is_set(unsigned char temp, int n) {
    unsigned char x;
    switch (n) {
        case 0:
            x = 128;
            break;
        case 1:
            x = 64;
            break;
        case 2:
            x = 32;
            break;
        case 3:
            x = 16;
            break;
        case 4:
            x = 8;
            break;
        case 5:
            x = 4;
        case 6:
            x = 2;
            break;
        case 7:
            x = 1;
            break;
        default:
            x = 0;
            break;

    }
    return temp & x;
}

int validate_utf8 (const char * x, int len) {
    unsigned char temp;
    int i = 0, length, j;
    while(i < len) {
        temp = (unsigned char) x[i];
        /*if (converted < 0) converted = 255 + converted;*/
        if (temp <= 127) {
            ++i;
        } else {

            if (temp >= 254) {
                return 0;
            } else if (temp >= 252) {
                length = 6;
            } else if (temp >= 248) {
                length = 5;
            } else if (temp >= 240) {
                length = 4;
            } else if (temp >= 224) {
                length = 3;
            } else if (temp >= 192) {
                length = 2;
            } else {
                return 0;
            }

            if (is_set(temp, length)) return 0;
            ++i;
            for (j = 0; j < length - 1; ++j) {
                if (i >= len) return 0;
                if (!is_set((unsigned char)x[i], 0) || is_set((unsigned char)x[i], 1)) {
                    return 0;
                }
                ++i;
            }

        }

    }
    return 1;
}

int char_type ( char x, int * length ) {
    int converted;

    if (x == ' '  || x == '\t' || x == '\n') return 1; /* White space */
    if (x == '.' || x == ',' || x == ':' || x == ';' || x == '?' || x == '!') return 2; /* Interpuntuation */
    if (x == '\0') return 3; /* THE END OF THE WORLD */

    converted = (int)x;
    *length = 1;
    if (converted >= 252) {
        *length = 6;
    }
    else if (converted >= 248) {
        *length = 5;
    }
    else if (converted >= 240) {
        *length = 4;
    }
    else if (converted >= 224) {
        *length = 3;
    }
    else if (converted >= 192) {
        *length = 2;
    }

    /*printf("Length: %d (%d)\n", *length, converted);*/

    return 4; /* Text */
}

int skip_white_space ( const char * text, int * current_char ) {
    int type;
    int newlines = 0;
    int char_length;

    while(1) {
        type = char_type(text[*current_char], &char_length);
        if (type == 3) return 0;
        else if (type == 1) {
            if (text[*current_char] == '\n') newlines+=1;
            *current_char += 1;
        } else {
            break;
        }
    }

    /*printf("%d\n", newlines);*/

    if (newlines >= 2) return 2; /* New paragraph */

    return 1; /* OK */
}

int next_word_length ( const char * text, int current_char, int * word_length, int * ends_with_punctuation) {
    int length = 0, newline = 0, res, char_length;
    res = skip_white_space( text, &current_char);
    if (res == 0) return 0;
    else if (res == 2) newline = 1;

    while(char_type(text[current_char], &char_length) == 4) {
        length += 1;
        current_char += char_length;
    }

    if (length == 0) {
        return 3;
    }

    res = skip_white_space(text, &current_char);
    if (res == 0 || res == 2) {
        *word_length = length;
        *ends_with_punctuation = 0;
        return 1;
    }
    if (char_type(text[current_char], &char_length) == 2) {
        *word_length = length + 1;
        *ends_with_punctuation = 1;
    } else {
        *word_length = length;
        *ends_with_punctuation = 0;
    }

    if (newline == 1) {
        return 2;
    } else {
        return 1;
    }

}

void end_line(TDOCUMENT * document, int current_page, int current_row, int current_column) {
    document->m_Pages[current_page].m_Lines[current_row][current_column] = '\0';
}

void new_line(TDOCUMENT * document, int page_width, int page_rows, int * current_page, int * current_row, int * current_column, int * current_length ) {
    end_line(document, *current_page, *current_row, *current_column);
    if (*current_row + 1 >= page_rows) {
        add_page(document, page_width, page_rows);
        *current_page += 1;
        *current_row = 0;
        *current_column = 0;
    } else {
        *current_row += 1;
        *current_column = 0;

    }
    *current_length = 0;
    document->m_Pages[*current_page].m_Lines[*current_row] = (char *) malloc((page_width+1) * 6 * sizeof(char));
    document->m_Pages[*current_page].m_LinesNr += 1;
}

void DocFree ( TDOCUMENT  * x ) {
    int i, j;
    if (x->m_PagesNr > 0) {
        for (i = 0; i < x->m_PagesNr; ++i) {
            if (x->m_Pages[i].m_LinesNr > 0) {
                for (j = 0; j < x->m_Pages[i].m_LinesNr; ++j) {
                    free(x->m_Pages[i].m_Lines[j]);
                }
            }
            free(x->m_Pages[i].m_Lines);
        }
        free(x->m_Pages);
    }
    free(x);
}


TDOCUMENT * DocFormat ( int pageWidth,
                        int pageRows,
                        const char * text ) {
    TDOCUMENT * document = NULL;
    int string_length = (int) strlen(text);
    int current_char = 0,
        current_page = 0,
        current_row = 0,
        current_column = 0,
        current_length = 0,
        new_paragraph = 1,
        word_length = 0,
        ends_with_punctuation = 0,
        res,
        punctuation_at_the_end = 0,
        char_length, i;

    if (pageWidth == 0 || pageRows == 0 || validate_utf8(text, string_length) == 0 ) return NULL;


    document = (TDOCUMENT *) malloc(sizeof(TDOCUMENT));

    document->m_PagesNr = 0;
    document->m_Pages = NULL;

    add_page(document, pageWidth, pageRows);

    document->m_Pages[0].m_Lines[0] = (char *) malloc((pageWidth+1) * 6 * sizeof(char));
    document->m_Pages[0].m_LinesNr += 1;

    while( current_char < string_length) {
        if (new_paragraph == 1) {
            document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
            document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
            document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
            current_length += 3;

            res = next_word_length(text, current_char, &word_length, &ends_with_punctuation);
            if (res == 0) {
                end_line(document, current_page, current_row, current_column);
                break;
            } else if (res == 3) {
                DocFree(document);
                return NULL;
            }

            if (3 + word_length > pageWidth ) /* two spaces + \0 */ {
                DocFree(document);
                return NULL;
            }

            skip_white_space(text, &current_char);
            while(char_type(text[current_char], &char_length) == 4) {
                for (i = 0; i < char_length; ++i) {
                    document->m_Pages[current_page].m_Lines[current_row][current_column++] = text[current_char++];
                }
                current_length += 1;
            }

            if (ends_with_punctuation) {
                res = skip_white_space(text, &current_char);
                if (res == 0) {
                    end_line(document, current_page, current_row, current_column);
                    break;
                }
                document->m_Pages[current_page].m_Lines[current_row][current_column++] = text[current_char++];
                ++current_length;
                punctuation_at_the_end = 1;
            } else {
                punctuation_at_the_end = 0;
            }

            new_paragraph = 0;



        } else {
            res = next_word_length(text, current_char, &word_length, &ends_with_punctuation);
            if (res == 0) {
                end_line(document, current_page, current_row, current_column);
                break;
            } else if (res == 3) {
                DocFree(document);
                return NULL;
            }

            if (word_length > pageWidth) {
                DocFree(document);
                return NULL;
            }

            if (punctuation_at_the_end == 1) {
                if (word_length + 2 + current_length > pageWidth) {
                    new_line(document, pageWidth, pageRows, &current_page, &current_row, &current_column, &current_length);
                    punctuation_at_the_end = 0;
                }
            } else {
                if (word_length + current_length + 1 > pageWidth) {
                    new_line(document, pageWidth, pageRows, &current_page, &current_row, &current_column, &current_length);
                }
            }
            if (punctuation_at_the_end == 1) {
                document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
                document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
                current_length += 2;
                punctuation_at_the_end = 0;
            } else if (current_column) {
                document->m_Pages[current_page].m_Lines[current_row][current_column++] = ' ';
                ++current_length;
            }

            skip_white_space(text, &current_char);
            while(char_type(text[current_char], &char_length) == 4) {
                for (i = 0; i < char_length; ++i) {
                    document->m_Pages[current_page].m_Lines[current_row][current_column++] = text[current_char++];
                }
                ++current_length;
            }
            if (ends_with_punctuation) {
                res = skip_white_space(text, &current_char);
                if (res == 0) {
                    end_line(document, current_page, current_row, current_column);
                    break;
                }
                document->m_Pages[current_page].m_Lines[current_row][current_column++] = text[current_char++];
                ++current_length;
                punctuation_at_the_end = 1;
            }

        }

        res = skip_white_space(text, &current_char);
        /*printf("%d at page %d row %d column %d\n", res, current_page, current_row, current_column);*/

        if (res == 0) {
            end_line(document, current_page, current_row, current_column);
            break;
        }
        if (res == 2) {
            new_paragraph = 1;
            new_line(document, pageWidth, pageRows, &current_page, &current_row, &current_column, &current_length);
            new_line(document, pageWidth, pageRows, &current_page, &current_row, &current_column, &current_length);
        }

        /*++current_char;*/
    }

    return document;
}



#ifndef __PROGTEST__

void print_document(TDOCUMENT * document, int length) {
    int i, j;
    for (i = 0; i <= document->m_PagesNr -1; ++i) {
        for (j = 0; j < document->m_Pages[i].m_LinesNr; j++) {
            if ( document->m_Pages[i].m_Lines[j] ) printf("%s\n", document->m_Pages[i].m_Lines[j]);
        }
        for (j = 0; j < length; ++j) {
            printf("-");
        }
        printf("\n");
    }
    printf("--------------------------------------\n");
}



void print_str(char * text) {
    int i;
    printf("%s:\n", text);
    for (i = 0; i < (int) strlen(text) + 1; ++i) {
        if (text[i] < 0)  printf("%d ", 255+text[i]);
        else printf("%d ", text[i]);
    }
    printf("\n");
}

int main ( int argc, char * argv [] ) {
    TDOCUMENT * doc;

    doc = DocFormat ( 30, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );


    assert ( doc -> m_PagesNr == 5 );
    assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "consectetur adipiscing elit." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "In tincidunt posuere diam in" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "elementum.  Proin viverra" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "risus nibh,  eu ornare est" ) == 0 );
    assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "placerat ut.  Maecenas nec" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "nisl non elit pharetra" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "finibus.  Nunc ut rutrum enim." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "Mauris congue,  neque quis" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "porta porta,  augue erat" ) == 0 );
    assert ( doc -> m_Pages[2] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "sodales ligula,  ac blandit" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "risus odio quis tellus." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "   Sed a dictum eros,  sed" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "posuere nibh.  Nulla sodales" ) == 0 );
    assert ( doc -> m_Pages[3] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[0], "sit amet nunc vestibulum" ) == 0 );

    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[1], "rutrum.  Vivamus venenatis," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[2], "quam et iaculis bibendum," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[3], "dolor felis pretium dolor,  at" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[4], "viverra lacus erat in sapien." ) == 0 );
    assert ( doc -> m_Pages[4] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[4] . m_Lines[0], "Donec accumsan nulla massa," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[4] . m_Lines[1], "sed facilisis nulla efficitur" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[4] . m_Lines[2], "at.  Sed tellus tellus," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[4] . m_Lines[3], "mattis et pulvinar vel," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[4] . m_Lines[4], "dignissim at nulla." ) == 0 );
    DocFree ( doc );

    doc = DocFormat ( 50, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n"  );
    assert ( doc -> m_PagesNr == 4 );
    assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet,  consectetur" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "adipiscing elit.  In tincidunt posuere diam in" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "elementum.  Proin viverra risus nibh,  eu ornare" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "est placerat ut.  Maecenas nec nisl non elit" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "pharetra finibus.  Nunc ut rutrum enim.  Mauris" ) == 0 );
    assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "congue,  neque quis porta porta,  augue erat" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "sodales ligula,  ac blandit risus odio quis" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "tellus." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "   Sed a dictum eros,  sed posuere nibh.  Nulla" ) == 0 );
    assert ( doc -> m_Pages[2] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "sodales sit amet nunc vestibulum rutrum.  Vivamus" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "venenatis,  quam et iaculis bibendum,  dolor felis" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "pretium dolor,  at viverra lacus erat in sapien." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "Donec accumsan nulla massa,  sed facilisis nulla" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "efficitur at.  Sed tellus tellus,  mattis et" ) == 0 );
    assert ( doc -> m_Pages[3] . m_LinesNr == 1 );
    assert ( strcmp ( doc -> m_Pages[3] . m_Lines[0], "pulvinar vel,  dignissim at nulla." ) == 0 );
    DocFree ( doc );

    doc = DocFormat ( 80, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );
    assert ( doc -> m_PagesNr == 3 );
    assert ( doc -> m_Pages[0] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor sit amet,  consectetur adipiscing elit.  In tincidunt" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "posuere diam in elementum.  Proin viverra risus nibh,  eu ornare est placerat" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "ut.  Maecenas nec nisl non elit pharetra finibus.  Nunc ut rutrum enim.  Mauris" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "congue,  neque quis porta porta,  augue erat sodales ligula,  ac blandit risus" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "odio quis tellus." ) == 0 );
    assert ( doc -> m_Pages[1] . m_LinesNr == 5 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "   Sed a dictum eros,  sed posuere nibh.  Nulla sodales sit amet nunc vestibulum" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "rutrum.  Vivamus venenatis,  quam et iaculis bibendum,  dolor felis pretium" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "dolor,  at viverra lacus erat in sapien.  Donec accumsan nulla massa,  sed" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "facilisis nulla efficitur at.  Sed tellus tellus,  mattis et pulvinar vel," ) == 0 );
    assert ( doc -> m_Pages[2] . m_LinesNr == 1 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "dignissim at nulla." ) == 0 );
    DocFree ( doc );
    doc = DocFormat ( 20, 15, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" );
    assert ( doc -> m_PagesNr == 3 );
    assert ( doc -> m_Pages[0] . m_LinesNr == 15 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem ipsum dolor" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "sit amet," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "consectetur" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[3], "adipiscing elit.  In" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[4], "tincidunt posuere" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[5], "diam in elementum." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[6], "Proin viverra risus" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[7], "nibh,  eu ornare est" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[8], "placerat ut." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[9], "Maecenas nec nisl" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[10], "non elit pharetra" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[11], "finibus.  Nunc ut" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[12], "rutrum enim.  Mauris" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[13], "congue,  neque quis" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[14], "porta porta,  augue" ) == 0 );
    assert ( doc -> m_Pages[1] . m_LinesNr == 15 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[0], "erat sodales ligula," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[1], "ac blandit risus" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[2], "odio quis tellus." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[3], "" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[4], "   Sed a dictum" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[5], "eros,  sed posuere" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[6], "nibh.  Nulla sodales" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[7], "sit amet nunc" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[8], "vestibulum rutrum." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[9], "Vivamus venenatis," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[10], "quam et iaculis" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[11], "bibendum,  dolor" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[12], "felis pretium dolor," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[13], "at viverra lacus" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[1] . m_Lines[14], "erat in sapien." ) == 0 );
    assert ( doc -> m_Pages[2] . m_LinesNr == 8 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[0], "Donec accumsan nulla" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[1], "massa,  sed" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[2], "facilisis nulla" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[3], "efficitur at.  Sed" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[4], "tellus tellus," ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[5], "mattis et pulvinar" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[6], "vel,  dignissim at" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[2] . m_Lines[7], "nulla." ) == 0 );
    DocFree ( doc );

    assert ( DocFormat ( 10, 5, "Lorem   ipsum dolor sit amet,\n"
        "consectetur adipiscing elit. In\ttincidunt posuere\n"
        "diam in elementum. Proin viverra risus nibh,\n"
        "eu ornare est placerat ut. Maecenas nec nisl\n"
        "non elit pharetra finibus.   Nunc ut rutrum enim.\n"
        "Mauris congue       ,neque quis porta porta,\n"
        "augue erat sodales ligula, ac blandit risus odio\n"
        "quis tellus.\n"
        "  \n"
        " Sed a dictum eros  ,  sed posuere nibh.\n"
        "Nulla sodales sit amet nunc vestibulum rutrum.\n"
        "      Vivamus venenatis, quam et iaculis bibendum,\n"
        "dolor felis pretium dolor,at viverra lacus erat in\n"
        "sapien. Donec accumsan nulla massa, sed facilisis\n"
        "nulla efficitur at.Sed tellus tellus, mattis et\n"
        "pulvinar vel, dignissim at nulla.\n" ) == NULL );
    assert ( DocFormat ( 8, 5, "Lorem." ) == NULL );
    assert ( DocFormat ( 15, 5, "Lorem.." ) == NULL );
    doc = DocFormat (20, 5, "\n\nLorem.\n\nLorem.\n\n" );

    assert ( doc -> m_PagesNr == 1);
    assert ( doc -> m_Pages[0].m_LinesNr == 3);
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[0], "   Lorem." ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[1], "" ) == 0 );
    assert ( strcmp ( doc -> m_Pages[0] . m_Lines[2], "   Lorem." ) == 0 );
    DocFree(doc);

    assert(is_set(1, 0) == 0);
    assert(is_set(1, 7));

    assert(validate_utf8("National jí etnické vidět v ekologickou významem",
                         (int) strlen("National jí etnické vidět v ekologickou významem")) == 1);
    assert(validate_utf8("W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o",
                         (int) strlen("W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o")) == 0);



    assert(validate_utf8("�" , (int) strlen("�")) == 1);
    assert(validate_utf8("" , (int) strlen("")) == 1);
    assert(validate_utf8("ࠀ" , (int) strlen("ࠀ")) == 1);
    assert(validate_utf8("𐀀" , (int) strlen("𐀀")) == 1);
    assert(validate_utf8("�����" , (int) strlen("�����")) == 1);
    assert(validate_utf8("������" , (int) strlen("������")) == 1);
    assert(validate_utf8("" , (int) strlen("")) == 1);
    assert(validate_utf8("߿" , (int) strlen("߿")) == 1);
    assert(validate_utf8("￿" , (int) strlen("￿")) == 1);
    assert(validate_utf8("����" , (int) strlen("����")) == 1);
    assert(validate_utf8("�����" , (int) strlen("�����")) == 1);
    assert(validate_utf8("������" , (int) strlen("������")) == 1);
    assert(validate_utf8("퟿" , (int) strlen("퟿")) == 1);
    assert(validate_utf8("" , (int) strlen("")) == 1);
    assert(validate_utf8("�" , (int) strlen("�")) == 1);
    assert(validate_utf8("􏿿" , (int) strlen("􏿿")) == 1);
    assert(validate_utf8("����" , (int) strlen("����")) == 1);
    

    doc = DocFormat(30, 10, "National jí etnické vidět v ekologickou významem vykreslují lišit poklidné,"
            "mj. i. Hlavu nutně centrem cítíte dál rozdíl, sice mizí od tj. úrovně po dinosaur poznáte do funkce?"
            "Boky v eroze zimami světový hodlá sdružení – plyne geologicky duší kurzy.\n\n"
            "Skryté mě odhadů posoudit o délky dosahující argumenty, té kouzlo nádorovité z mozkovou sebevýkonnější v polopotopenou virům."
            "Předpokládané mysu teorií ze sem, na kterou pokračují sportech šedá pozdního takovou z zredukované,"
            "daří nemyslící minuty si pokroucených ovládaný, mraky ukazuje čím víkend veškeré."
            "Z této rozvojem důvodu – OSN ze nejhůře tohle přičemž od zadře matkou osamělá jakým?"
            "Mořem stěn tož s listopadovém tradiční, jádro Darwin začala po stádu nástrojů,"
            "tak lidském materiál ní psounů bubák gejzírů od ventilačními milion. ");

    assert(doc != NULL);

    /*print_document(doc, 30);*/




    DocFree(doc);

    assert( DocFormat ( 135, 54, "W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o" ) == NULL);

    /*print_str("W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o");*/

    /*printf("%d\n", is_set(255+(int)'\xf2', 1));*/

    /*printf("%d\n", validate_utf8("W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o", (int) strlen("W\xf2\xac\x98\x9csrr\xcf\x83\xd1\x8b\x97Rv\xcd\xb2py\xc2\x88wb\xd9\xb3o")));*/


    return 0;
 }
#endif /* __PROGTEST__ */
