#include <stdio.h>
#include <stdlib.h>

#define MAX_INT 2147483647
#define OPT_1 1.5212
#define OPT_2 2.75
#define OPT_3 14
#define OPT_4 3.4


typedef struct rslt_my_ass {
    int size;
    long long tree;
} result;

int * emissions = NULL;
int emissions_count = 0;
int emissions_allocated = 0;
int all_same = 1;

#ifndef __PROGTEST__
unsigned long long calls = 0;
/*unsigned long long opt_hits1 = 0;
unsigned long long opt_hits2 = 0;
unsigned long long opt_hits3 = 0;
unsigned long long opt_hits4 = 0;
unsigned long long opt_hits5 = 0;
unsigned long long opt_hits6 = 0;
unsigned long long opt_hits7 = 0;
unsigned long long opt_hits8 = 0;
unsigned long long opt_hits9 = 0;*/
#endif

int sum = 0;
double average;
int threes = 0;
int allow_threes = 1;
int best_possible;

int max_size = 0;
int current_best = MAX_INT;

int * emissions_a = NULL;
int emissions_a_count = 0;
int * emissions_b = NULL;
int emissions_b_count = 0;
int * emissions_c = NULL;
int emissions_c_count = 0;

void add_emission(int value) {
    if (emissions_count >= 1 && emissions[emissions_count-1] != value) all_same = 0;
    if (++emissions_count > emissions_allocated) {
        emissions_allocated = 2*emissions_count;
        emissions = (int *) realloc(emissions, emissions_allocated * sizeof(int));
    }
    emissions[emissions_count-1] = value;
}

int load_emissions( void ) {
    int res, val;
    printf("Emise ventilu:\n");
    while((res = scanf("%d", &val)) == 1) {
        if (val < 1) return 0;
        sum += val;
        add_emission(val);
    }

    if (res == -1 && emissions_count > 0) return 1;
    else return 0;

}

void print_wrong_input() {
    printf("Nespravny vstup.\n");
}

void free_everything() {
    free(emissions);
    free(emissions_a);
    free(emissions_b);
    free(emissions_c);
}

/*void print_emissions() {
    int i;
    if (emissions_count == 0) return;
    for (i = 0; i < emissions_count; ++i) {
        printf("%d, ", emissions[i]);
    }
    printf("\n");
}*/

int sort_function ( const int * a, const int * b ) {
    return ( *b - *a );
}


#ifndef __PROGTEST__
void print_solution() {
    /*int i;

    int sum;*/
    printf("Nejvyssi emise: %d\n", max_size);

    /*printf("A: ");
    sum = 0;
    if (emissions_a_count) for (i = 0; i < emissions_a_count; ++i) {
        if (i != 0) printf(", ");
        printf("%d", emissions_a[i]);
        sum += emissions_a[i];
    }
    printf(" - %d", sum);
    printf("\n");

    printf("B: ");
    sum = 0;
    if (emissions_b_count) for (i = 0; i < emissions_b_count; ++i) {
        if (i != 0) printf(", ");
        printf("%d", emissions_b[i]);
        sum += emissions_b[i];
    }
    printf(" - %d", sum);
    printf("\n");

    printf("C: ");
    sum = 0;
    if (emissions_c_count) for (i = 0; i < emissions_c_count; ++i) {
        if (i != 0) printf(", ");
        printf("%d", emissions_c[i]);
        sum += emissions_c[i];
    }
    printf(" - %d", sum);
    printf("\n");*/
}

#else

void print_solution() {
    int i;

    printf("Nejvyssi emise: %d\n", max_size);

    printf("A: ");
    if (emissions_a_count) for (i = 0; i < emissions_a_count; ++i) {
            if (i != 0) printf(", ");
            printf("%d", emissions_a[i]);
        }
    printf("\n");

    printf("B: ");
    if (emissions_b_count) for (i = 0; i < emissions_b_count; ++i) {
            if (i != 0) printf(", ");
            printf("%d", emissions_b[i]);
        }
    printf("\n");

    printf("C: ");
    if (emissions_c_count) for (i = 0; i < emissions_c_count; ++i) {
            if (i != 0) printf(", ");
            printf("%d", emissions_c[i]);
        }
    printf("\n");
}
#endif

result tryOption( int * array, int count, int sum_a, int sum_b, int sum_c, int count_a, int count_b, int count_c, long long id, int available_threes) {
    int depth = emissions_count - count;
    result res_a, res_b, res_c;

#ifndef __PROGTEST__
    ++calls;
#endif

    if (count == 0) {
        result rslt;
        rslt.size = (sum_a > sum_b) ? (sum_a > sum_c ? sum_a : sum_c) : (sum_b > sum_c ? sum_b : sum_c);
        if (current_best > rslt.size) current_best = rslt.size;
        rslt.tree = id;
        return rslt;
    }

    if (allow_threes && available_threes > 0 && count > 3 && array[0] == array[1] && array[1] == array[2]) {
        return tryOption ( array + 3, count - 3,
                           sum_a + array[0], sum_b + array[1], sum_c + array[2],
                           ++count_a, ++count_b, ++count_c,
                           3 * (3 * (3*id + 1) + 2 ) + 3, available_threes - 1 );
    }

    if (depth > 3) {

        if ( ( current_best < sum_a + *array ) ||
             ( count_a > OPT_3 ) ||
             ( ( count_a && count_a > emissions_count / OPT_4 && ( sum_a * 1.0 ) / (count_a * 1.0) > average ) ) ||
             ( sum_a * OPT_1 > sum_b + sum_c ) ||
             ( count_a > count_c * OPT_2 ) /*||
             ( sum_c < *array ) ||
             ( sum_b < *array ) ||
             ( sum_c < *array ) ||
             ( count_a > count_b * OPT_2 )*/
        ) {
            res_a.size = MAX_INT;
        } else {
            res_a = tryOption(array + 1, count - 1, sum_a + *array, sum_b, sum_c, ++count_a, count_b, count_c,
                              3 * id + 1, available_threes);

            if (res_a.size == best_possible) {
                return res_a;
            }
        }

        if ( ( current_best < sum_b + *array ) ||
             ( count_b > OPT_3 ) ||
             ( ( count_b && count_b > emissions_count / OPT_4 && ( sum_b * 1.0 ) / (count_b * 1.0) > average ) ) ||
             ( sum_b * OPT_1 > sum_a + sum_c ) ||
             ( count_b > count_c * OPT_2 ) /*||
             ( sum_c < *array ) ||
             ( sum_a < *array ) ||
             ( sum_c < *array ) ||
             ( count_b > count_a * OPT_2  )*/
        ) {
            res_b.size = MAX_INT;
        } else {
            res_b = tryOption ( array + 1, count - 1, sum_a, sum_b + *array, sum_c, count_a, ++count_b, count_c,
                                3*id + 2, available_threes );

            if (res_b.size == best_possible) {
                return res_b;
            }
        }


        if ( ( current_best < sum_c + *array ) ||
             ( count_c > OPT_3 ) ||
             ( ( count_c && count_c > emissions_count / OPT_4 && ( sum_c * 1.0 ) / (count_c * 1.0) > average ) ) ||
             ( sum_c * OPT_1 > sum_b + sum_a ) ||
             ( count_c > count_b * OPT_2 )/* ||
             ( sum_b < *array ) ||
             ( sum_a < *array ) ||
             ( sum_b < *array ) ||
             ( count_c > count_a * OPT_2  )*/
        ) {
            res_c.size = MAX_INT;
        } else {
            res_c = tryOption ( array + 1, count - 1, sum_a, sum_b, sum_c + *array, count_a, count_b, ++count_c,
                                3*id + 3, available_threes );

            if (res_c.size == best_possible) {
                return res_c;
            }
        }

    } else {
        res_a = tryOption ( array + 1, count - 1, sum_a + *array, sum_b, sum_c, ++count_a, count_b, count_c, 3*id + 1, available_threes );
        res_b = tryOption ( array + 1, count - 1, sum_a, sum_b + *array, sum_c, count_a, ++count_b, count_c, 3*id + 2, available_threes );
        res_c = tryOption ( array + 1, count - 1, sum_a, sum_b, sum_c + *array, count_a, count_b, ++count_c, 3*id + 3, available_threes );

    }

    return (res_a.size < res_b.size) ? (res_a.size < res_c.size ? res_a : res_c) : (res_b.size < res_c.size ? res_b : res_c);


}

int main ( void ) {

    if (!load_emissions()) {
        print_wrong_input();
        free_everything();
        return 0;

    }

    qsort ( emissions, (size_t) emissions_count, sizeof ( *emissions ),
            (int(*)(const void *, const void *)) sort_function );

    emissions_a = (int *) malloc(emissions_count * sizeof(int));
    emissions_b = (int *) malloc(emissions_count * sizeof(int));
    emissions_c = (int *) malloc(emissions_count * sizeof(int));

    if (emissions_count % 3 == 0) {
        int i = 0;
        while (i < emissions_count) {
            int tmp1 = emissions[i];
            int tmp2 = emissions[i+1];
            int tmp3 = emissions[i+2];
            if (tmp1 == tmp2 && tmp2 == tmp3) {
                ++threes;
                i += 3;
            } else ++i;
        }
    }

    if (emissions_count == 1) {
        max_size = emissions[0];
        emissions_a[emissions_a_count++] = emissions[0];
    } else if (emissions_count == 2) {
        max_size = emissions[0];
        emissions_a[emissions_a_count++] = emissions[0];
        emissions_b[emissions_b_count++] = emissions[1];
    } else if (emissions_count == 3) {
        max_size = emissions[0];
        emissions_a[emissions_a_count++] = emissions[0];
        emissions_b[emissions_b_count++] = emissions[1];
        emissions_c[emissions_c_count++] = emissions[2];
    } else if (emissions_count == 4) {
        max_size = emissions[0];
        emissions_a[emissions_a_count++] = emissions[0];
        emissions_b[emissions_b_count++] = emissions[1];
        emissions_c[emissions_c_count++] = emissions[2];
        emissions_c[emissions_c_count++] = emissions[3];
        if (emissions[2] + emissions[3] > max_size) max_size = emissions[2] + emissions[3];
    } else if (all_same) {
        int i;
        for (i = 0; i < emissions_count; ++i) {
            if (i % 3 == 0) emissions_a[emissions_a_count++] = emissions[i];
            else if (i % 2 == 0) emissions_b[emissions_b_count++] = emissions[i];
            else emissions_c[emissions_c_count++] = emissions[i];
        }


        max_size = emissions[0] * ((emissions_a_count > emissions_b_count) ?
                                   (emissions_a_count > emissions_c_count ? emissions_a_count : emissions_c_count)
                                                                           : (emissions_b_count > emissions_c_count ? emissions_b_count : emissions_c_count));

    } else if (threes * 3 == emissions_count) {
        int i, sum_a = 0, sum_b = 0, sum_c = 0;
        for (i = 0; i < emissions_count; ++i) {
            if (i % 3 == 0) {
                emissions_a[emissions_a_count++] = emissions[i];
                sum_a += emissions[i];
            }
            else if (i % 2 == 0) {
                emissions_b[emissions_b_count++] = emissions[i];
                sum_b += emissions[i];
            }
            else {
                emissions_c[emissions_c_count++] = emissions[i];
                sum_c += emissions[i];
            }
        }

        max_size = sum_a > sum_b ? (sum_a > sum_c ? sum_a : sum_c) : (sum_b > sum_c ? sum_b : sum_c);
    } else {
        int j = emissions_count-1, val, x;
        long long int i;
        result rslt;
        result greedy;
        int sum_a = 0, sum_b = 0, sum_c = 0;
        best_possible = (sum + 2)/3;
        greedy.tree = 0;

        average = ( sum * 1.0 ) / (emissions_count * 1.0 );
        allow_threes = sum - emissions[0] < emissions[0] ? 0 : 1;

        for (x = 0; x < emissions_count; ++x) {
            int min = sum_a < sum_b ? (sum_a < sum_c ? sum_a : sum_c) : (sum_b < sum_c ? sum_b : sum_c);
            if (min == sum_a) {
                sum_a += emissions[x];
                greedy.tree = greedy.tree * 3 + 1;
            } else if (min == sum_b) {
                sum_b += emissions[x];
                greedy.tree = greedy.tree * 3 + 2;
            } else  {
                sum_c += emissions[x];
                greedy.tree = greedy.tree * 3 + 3;
            }
        }
        greedy.size = sum_a > sum_b ? (sum_a > sum_c ? sum_a : sum_c) : (sum_b > sum_c ? sum_b : sum_c);

        current_best = greedy.size;

        if (current_best == best_possible) {
            rslt = greedy;
        } else {

            if (threes > 0 && emissions[0] == emissions[1] && emissions[1] == emissions[2]) {
                rslt = tryOption ( emissions + 3, emissions_count - 3, emissions[0], emissions[1], emissions[2], 1, 1, 1, 18, threes - 1);
            } else {
                rslt = tryOption ( emissions + 1, emissions_count - 1, emissions[0],            0,            0, 1, 0, 0, 1, threes);
            }

            rslt = rslt.size > greedy.size ? greedy : rslt;
        }

        max_size = rslt.size;
        /*fprintf(stderr, "Best possible %d Greedy %d Max size %d\n", best_possible, greedy.size, max_size);*/


        for ( i = rslt.tree; i > 0; i=(i-1)/3 ){
            val = emissions[j--];
            switch (i % 3){
                case 0:
                    emissions_a[emissions_a_count++] = val;
                    break;
                case 1:
                    emissions_b[emissions_b_count++] = val;
                    break;
                case 2:
                    emissions_c[emissions_c_count++] = val;
                    break;
            }
        }
    }

    print_solution();

    free_everything();

#ifndef __PROGTEST__
    fprintf(stderr, "Calls: %lld\n", calls);
    /*fprintf(stderr, "opt_hits1: %lld\n", opt_hits1);
    fprintf(stderr, "opt_hits2: %lld\n", opt_hits2);
    fprintf(stderr, "opt_hits3: %lld\n", opt_hits3);
    fprintf(stderr, "opt_hits4: %lld\n", opt_hits4);
    fprintf(stderr, "opt_hits5: %lld\n", opt_hits5);
    fprintf(stderr, "opt_hits6: %lld\n", opt_hits6);
    fprintf(stderr, "opt_hits7: %lld\n", opt_hits7);
    fprintf(stderr, "opt_hits8: %lld\n", opt_hits8);
    fprintf(stderr, "opt_hits9: %lld\n", opt_hits9);*/


#endif

    return 0;
}