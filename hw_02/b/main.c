#include <stdio.h>
#include <ctype.h>
#include <limits.h>

int ExtendedGcd(__int128_t a, __int128_t b, __int128_t *x, __int128_t *y)
{
    __int128_t newx, newy;
    if (a % b == 0)
    {
        *x = 0;
        *y = 1;
        return 1;
    }
    if (!ExtendedGcd(b, a % b, &newx, &newy)) {
        return 0;
    }

    *x = newy;
    *y = newx - newy * (a / b);
    return 1;
}


unsigned long long int my_gcd(unsigned long long int a, unsigned long long int b) {
    unsigned long long int tmp;

    if (a < b ) {
        tmp = a;
        a = b;
        b = tmp;
    }
    while (b > 0) {
        tmp = a % b;
        a = b;
        b = tmp;
    }
    return a;
}

int my_lcm(unsigned long long int a, unsigned long long int b, unsigned long long int * res) {
    /*printf("%llu\n"
           "%llu\n"
           "%llu\n" , a, b, ULLONG_MAX);*/
    if ((a > b && a > ULLONG_MAX / b) || b > ULLONG_MAX / a) {
        return 0;
    } else {
        *res = a*b/my_gcd(a, b);
        return 1;
    }

}

__int128_t my_abs(__int128_t a) {
    if (a < 0) {
        return a * -1;
    } else {
        return a;
    }
}


void print_solutions(long long int i) {
    printf("Celkem variant: %lld\n", i);
}

void print_solution(long long int size1, long long int used1, long long int size2, long long int used2) {
    printf("= %lld * %lld + %lld * %lld\n", size1, used1, size2, used2);
}

void print_no_solutions(void) {
    printf("Reseni neexistuje.\n");
}

void print_wrong_input(void) {
    printf("Nespravny vstup.\n");
}

int main(void) {
    long long int size1, size2, distance;
    unsigned long long int used1 = 0, used2 = 0, counter = 0, max_used, tmp, usize1, usize2, udistance, gcd;
    char mode, x;
    __int128_t x2, y2, y_coe = 0, x_coe = 0, cnt, div_a, div_b, div_c;

    printf("Delky koleji:\n");
    if (scanf("%lld %lld", &size1, &size2) != 2 || size1 <= 0 || size2 <= 0 || size1 == size2) {
        print_wrong_input();
        return 0;
    }

    while (!isspace((x = getchar()))) {
        print_wrong_input();
        return 0;
    }

    printf("Vzdalenost:\n");
    if (scanf(" %c %lld", &mode, &distance) != 2 || !(mode == '-' || mode == '+') || distance < 0) {
        /*printf("'%c'", mode);*/
        print_wrong_input();
        return 0;
    }

    while (!isspace((x = getchar()))) {
        print_wrong_input();
        return 0;
    }

    if (size1%2 == 0 && size2%2 == 0 && distance%2 != 0) {
        print_no_solutions();
        return 0;
    }

    usize1 = (unsigned long long int)size1;
    usize2 = (unsigned long long int)size2;
    udistance = (unsigned long long int)distance;
    gcd = my_gcd(usize1, usize2);

    if (usize1 == udistance) {
        ++counter;
        if (udistance%usize2==0) {
            ++counter;
        }
        if (mode == '+') {
            print_solution(usize1, 1, usize2, 0);
            if (counter == 2) {
                print_solution(usize1, 0, usize2, udistance/usize2);
            }
        }
        print_solutions(counter);
        return 0;
    }

    if (usize2 == udistance) {
        ++counter;
        if (udistance%usize1==0) {
            ++counter;
        }
        if (mode == '+') {
            print_solution(usize1, 0, usize2, 1);
            if (counter == 2) {
                print_solution(usize1, udistance/usize1, usize2, 0);
            }
        }
        print_solutions(counter);
        return 0;
    }



    if (udistance % gcd != 0) {
        print_no_solutions();
        return 0;
    }

    max_used = udistance/usize1;

    if (mode == '+') {
        for(used1 = 0; used1 <= max_used; ++used1) {
            tmp = used1*usize1;

            used2 = (udistance-tmp)/usize2;
            if (tmp + used2*usize2 == udistance) {
                print_solution(usize1, used1, usize2, used2);
                ++counter;
            }
        }
    } else {

        if (udistance == 0) {
            print_solutions(++counter);
            return 0;
        }


        div_c = (__int128_t)(udistance/gcd);
        div_a = (__int128_t)(usize1/gcd);
        div_b = (__int128_t)(usize2/gcd);

        if (!ExtendedGcd(div_a, div_b, &x2, &y2)) {
            print_no_solutions();
            return 0;
        }


        x2 *= div_c;
        y2 *= div_c;



        if (y2 < 0 && x2 < 0) {
            print_no_solutions();
            return 0;

        } else if (y2 < 0) {
            y_coe = (__int128_t)(usize1 / gcd);
            x_coe = (__int128_t)(usize2 / gcd * (-1));


            cnt = my_abs(y2 / y_coe);


            y2 += (cnt + 1) * y_coe;
            x2 += (cnt + 1) * x_coe;


        } else if (x2 < 0) {
            y_coe = (__int128_t)(usize1 / gcd * (-1));
            x_coe = (__int128_t)(usize2 / gcd);

            cnt = my_abs(x2 / x_coe);

            x2 += (cnt + 1) * x_coe;
            y2 += (cnt + 1) * y_coe;
        }


        if ((long long)y2 < 0 || (long long)x2 < 0) {

            x2 -= x_coe;
            y2 -= y_coe;


            if (y2 < 0 || x2 < 0) {
                print_no_solutions();
                return 0;
            }
        }

        used1 = (unsigned long long)x2;
        used2 = (unsigned long long)y2;


        counter = used2/(size1/gcd) + used1/(size2/gcd) + 1;



    }

    if (!counter) {
        print_no_solutions();
        return 0;
    }

    print_solutions(counter);


    return 0;
}

