#include <stdio.h>
#include <limits.h>
#include <float.h>

double max_size = 10000000.00F;
short int too_big = 0;

unsigned long long int my_gcd(unsigned long long int a, unsigned long long int b) {
    unsigned long long int tmp;

    if (a < b ) {
        tmp = a;
        a = b;
        b = tmp;
    }
    while (b > 0) {
        tmp = a % b;
        a = b;
        b = tmp;
    }
    return a;
}

int my_lcm(unsigned long long int a, unsigned long long int b, unsigned long long int * res) {
    /*printf("%llu\n"
           "%llu\n"
           "%llu\n" , a, b, ULLONG_MAX);*/
    if ((a > b && a > ULLONG_MAX / b) || b > ULLONG_MAX / a) {
        return 0;
    } else {
        *res = a*b/my_gcd(a, b);
        return 1;
    }

}


double my_fabs(double n) {
    if (n < 0) {
        return -1 * n;
    } else {
        return n;
    }
}


int validate_product_size(int a) {
/*
    printf("Validating %d\n", a);
*/

    if (a < 1 ||
        a > max_size*100) {

        /*printf("incorrect: \n"
                "%.20f\n"
               "%.20f\n", my_fabs(a*100-(int)(a*100)), 10 * FLT_EPSILON * a*100 );*/
        return 0;
    } else {
        /*printf("correct: \n"
                "%.20f\n"
               "%.20f\n", my_fabs(a*100-(int)(a*100)), 10 * FLT_EPSILON * a*100 );*/
        return 1;
    }
}

unsigned long long int get_product_box(unsigned long long int a, unsigned long long int b, unsigned long long int c, unsigned long long int * res) {
    unsigned long long int box;
    if (!my_lcm(a, b, &box) || !my_lcm(c, box, &box)) {
        return 0;
    }
    /* printf("Box is %llu\n", box); */
    if (box/100.0 > max_size) {

        return 0;
    } else {
        *res = box;
        return 1;
    }
}

int convert_char(char x) {
    /*printf("Converted %c into %d\n", x, (int)(x-'0'));*/
    return (int)(x-'0');
}

int valid_char(char x) {
    int converted = convert_char(x);
    if (converted < 0 || converted > 9) {
        /*printf("Invalid char %c\n", x);*/
        return 0;
    }
    return 1;
}

int get_single(unsigned long long int * res, int last) {
    int a, x;
    char a1, a2;

    x = scanf("%d.%c%c", &a, &a1, &a2);

    if (x == -1) {
        *res = 12345678988;
        return 3;
    }

    /*printf("%d.%c%c\n", a, a1, a2);*/

    if (x != 3 ||
        !(valid_char(a1) && valid_char(a2)) ||
        !validate_product_size(a*100+convert_char(a1)*10+convert_char(a2))) {
        return 2;
    } else {
        *res = a*100+convert_char(a1)*10+convert_char(a2);
        return 1;
    }
}


int get_product(unsigned long long int * a_res, unsigned long long int * b_res, unsigned long long int * c_res) {
    int res1, res2, res3;

    res1 = get_single(a_res, 0);
    res2 = get_single(b_res, 0);
    res3 = get_single(c_res, 1);

    if (res1 == 2 || res2 == 2 || res3 == 2) {
        return 2;
    } else if (res1 == 3) {
        return 1;
    } else if (res2 == 3 || res3 == 3) {
        return 2;
    }

    /*printf("%llu %llu %llu\n", *a_res, *b_res, *c_res);*/

    return 0;

}



int main(void) {
    unsigned long long int final_box = 0;
    int product_return;
    unsigned long long int current_a, current_b, current_c, current_box;

    printf("Velikost vyrobku:\n");
    while(1) {
        product_return = get_product(&current_a, &current_b, &current_c);
        if (product_return == 1) {
            break;
        } else if (product_return == 2) {
            printf("Nespravny vstup.\n");
            return 0;
        } else {
            if (!get_product_box(current_a, current_b, current_c, &current_box)) {
                too_big = 1;
            } else {
                if (final_box == 0) {
                    final_box = current_box;
                } else {
                    if (!my_lcm(final_box, current_box, &final_box)) {
                        printf("Krabice je prilis velka.\n");
                        return 0;
                    }

                }
            }

        }
    }
    if (final_box == 0 && !too_big) {
        printf("Nespravny vstup.\n");
    } else if (final_box/100  > max_size || too_big) {
        printf("Krabice je prilis velka.\n");
    } else {
        printf("Velikost krabice: %.2f\n", final_box/100.0);
    }


    return 0;
}

