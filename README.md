# BI-PA1 homeworks

You should probably only use this as a reference - it would be kinda... obvious.
Licensed under MIT (see LICENSE) except for files in all the ``assignments`` folders.

I did not do all the individual homeworks, sometimes I only did those with higher scores.
If there was some progress in scores, the versions are in the commit history.
The full score is always the last.

The ``testall.sh`` file contains a script that can automatically test all inputs/outputs.
